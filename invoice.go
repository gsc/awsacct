package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
	"github.com/gocarina/gocsv"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

const (
	DateTimeFormat = "2006/01/02 15:04:05"
)

type DateTime struct {
	time.Time
}

func (dt *DateTime) MarshalCSV() (csv string, err error) {
	csv = dt.Time.Format(DateTimeFormat)
	return
}

func (dt *DateTime) UnmarshalCSV(csv string) (err error) {
	if csv == `` {
		dt.Time = time.Unix(0, 0)
	} else {
		dt.Time, err = time.Parse(DateTimeFormat, csv)
	}
	return
}

type TailReader struct {
	io.Reader
	IgnoreLines	int
}

func NewTailReader(in io.Reader, ignore int) *TailReader {
	return &TailReader{
		Reader: in,
		IgnoreLines: ignore,
	}
}

func (tr *TailReader) Read(p []byte) (int, error) {
Outer:
	for {
		n, err := tr.Reader.Read(p)
		if n > 0 {
			for tr.IgnoreLines > 0 {
				if i := bytes.IndexByte(p, '\n'); i == -1 {
					continue Outer
				} else {
					copy(p, p[i+1:])
					n -= i+1
					tr.IgnoreLines--
					if n == 0 {
						if err == nil {
							continue Outer
						} else {
							break
						}
					}
				}
			}
		}
		return n, err
	}
}

type TailWriter struct {
	io.Writer
	IgnoreLines	int
}

func NewTailWriter(out io.Writer, ignore int) *TailWriter {
	return &TailWriter{
		Writer: out,
		IgnoreLines: ignore,
	}
}

func (tw *TailWriter) Write(p []byte) (n int, e error) {
	n = len(p)
	for tw.IgnoreLines > 0 {
		if i := bytes.IndexByte(p, '\n'); i == -1 {
			return
		} else {
			p = p[i+1:]
			tw.IgnoreLines--
		}
	}

	if len(p) > 0 {
		var k int
		k, e = tw.Writer.Write(p)
		n -= len(p) - k
	}

	return
}

type Amount float64

func (a *Amount) MarshalCSV() (string, error) {
	return strconv.FormatFloat(float64(*a), 'f', 2, 64), nil
}

func (a *Amount) UnmarshalCSV(s string) (err error) {
	if s == `` {
		*a = 0
	} else {
		val, err := strconv.ParseFloat(s, 64)
		if err == nil {
			*a = Amount(val)
		}
	}
	return
}

type AWSInvoice struct {
	InvoiceID		string		`csv:"InvoiceID"`
	PayerAccountId		string		`csv:"PayerAccountId"`
	LinkedAccountId		string		`csv:"LinkedAccountId"`
	RecordType		string		`csv:"RecordType"`
	RecordID		string		`csv:"RecordID"`
	BillingPeriodStartDate	DateTime	`csv:"BillingPeriodStartDate"`
	BillingPeriodEndDate	DateTime	`csv:"BillingPeriodEndDate"`
	InvoiceDate		DateTime	`csv:"InvoiceDate"`
	PayerAccountName	string		`csv:"PayerAccountName"`
	LinkedAccountName	string		`csv:"LinkedAccountName"`
	TaxationAddress		string		`csv:"TaxationAddress"`
	PayerPONumber		int		`csv:"PayerPONumber"`
	ProductCode		string		`csv:"ProductCode"`
	ProductName		string		`csv:"ProductName"`
	SellerOfRecord		string		`csv:"SellerOfRecord"`
	UsageType		string		`csv:"UsageType"`
	Operation		string		`csv:"Operation"`
	AvailabilityZone	string		`csv:"AvailabilityZone"`
	RateID			int		`csv:"RateId"`
	ItemDescription		string		`csv:"ItemDescription"`
	UsageStartDate		DateTime	`csv:"UsageStartDate"`
	UsageEndDate		DateTime	`csv:"UsageEndDate"`
	UsageQuantity		Amount		`csv:"UsageQuantity"`
	BlendedRate		Amount		`csv:"BlendedRate"`
	CurrencyCode		string		`csv:"CurrencyCode"`
	CostBeforeTax		Amount		`csv:"CostBeforeTax"`
	Credits			Amount		`csv:"Credits"`
	TaxAmount		Amount		`csv:"TaxAmount"`
	TaxType			string		`csv:"TaxType"`
	TotalCost		Amount		`csv:"TotalCost"`
	BillingID		string		`csv:"user:billing"`
}

func ReadAWSInvoice(rd io.Reader) (invoice []*AWSInvoice, err error) {
	err = gocsv.Unmarshal(NewTailReader(rd, 1), &invoice)
	return
}

func ReadAWSInvoiceFromFile(file_name string) (invoice []*AWSInvoice, err error) {
	file, err := os.Open(file_name)
	if err != nil {
		return
	}
	defer file.Close()
	return ReadAWSInvoice(file)
}

func ReadAWSInvoiceFromS3Bucket(file_name string) (invoice []*AWSInvoice, err error) {
	client := s3.NewFromConfig(Conf.awsconf)
	obj, err := client.GetObject(context.TODO(),
		&s3.GetObjectInput{
			Bucket: aws.String(Conf.BucketName),
			Key: aws.String(file_name),
		})
	if err != nil {
		return
	}
	defer obj.Body.Close()
	return ReadAWSInvoice(obj.Body)
}

func ReadAWSInvoiceForMonth(year, month int) (invoice []*AWSInvoice, err error) {
	return ReadAWSInvoiceFromS3Bucket(fmt.Sprintf("%s-aws-cost-allocation-%04d-%02d.csv", Conf.accountID, year, month))
}

type AWSBillingEntry struct {
	ID			string		`csv:"id"`
	CostTax			Amount		`csv:"cost_tax"`
	CostNoTax		Amount		`csv:"cost_no_tax"`
	BillingID		string		`csv:"billing_id"`
	LinkedAccountName	string		`csv:"linked_account_name"`
	Count			int		`csv:"count"`
	appended		bool		`csv:"-"`
}

func AWSBilling(invoices []*AWSInvoice) (result []*AWSBillingEntry) {
	emap := map[string]*AWSBillingEntry{}
	for _, inv := range invoices {
		if inv.RecordType == `LinkedLineItem`  {
			key := inv.LinkedAccountName + `,` + inv.BillingID
			entry, found := emap[key]
			if !found {
				entry = &AWSBillingEntry{}
				emap[key] = entry

				entry.ID = inv.BillingID
				if entry.ID == `` {
					entry.ID = inv.LinkedAccountName
				}
				entry.BillingID = inv.BillingID
				entry.LinkedAccountName = inv.LinkedAccountName
			}
			entry.CostTax += inv.TotalCost
			entry.CostNoTax += inv.CostBeforeTax
			entry.Count++
			if !entry.appended && entry.CostNoTax >= 0.01 {
				result = append(result, entry)
				entry.appended = true
			}
		}
	}
	sort.Slice(result, func (i, j int) bool {
		li := strings.ToLower(result[i].LinkedAccountName)
		lj := strings.ToLower(result[j].LinkedAccountName)
		if li < lj {
			return true
		} else if li == lj {
			return result[i].CostNoTax > result[j].CostNoTax
		} else {
			return false
		}
	})
	return
}
