package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
	"github.com/gocarina/gocsv"
	"github.com/felixge/httpsnoop"
)

const (
	InvoicePathPrefix = "/invoice/"
	BillingPathPrefix = "/billing/"
)

func ServerActionHandler(args []string) {
	optset := NewBillOptSet(args)
	optset.SetParameters("[ADDRESS]")
	optset.Parse()
	args = optset.Args()

	addr := Conf.Address

	switch len(args) {
	case 0:
		// Nothing
	case 1:
		addr = args[0]
	default:
		log.Fatalln("bad arguments")
	}

	mux := &http.ServeMux{}

	mux.HandleFunc(InvoicePathPrefix,
		func (w http.ResponseWriter, r *http.Request) {
			path := strings.TrimPrefix(r.URL.Path, InvoicePathPrefix)
			InvoiceServerHandler(w, r, path, false)
		})
	mux.HandleFunc(BillingPathPrefix,
		func (w http.ResponseWriter, r *http.Request) {
			path := strings.TrimPrefix(r.URL.Path, BillingPathPrefix)
			InvoiceServerHandler(w, r, path, true)
		})
	
	hl := NewHTTPLogger()
	err := hl.OpenDefault()
	if err != nil {
		log.Println(err)
		hl.Open("-")
	}

	s := &http.Server{
		Addr: addr,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			m := httpsnoop.CaptureMetricsFn(w, func (w http.ResponseWriter) {
				if r.Method == http.MethodGet {
					mux.ServeHTTP(w, r)
				} else {
					errorHandler(w, r, http.StatusMethodNotAllowed)
				}
			})
			hl.Format(m, r)
		}),
	}
	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, syscall.SIGHUP, syscall.SIGTERM)
	go func() {
		log.Fatal(s.ListenAndServe())
	}()

sigloop:
	for {
		msg := <-signal_chan
		switch msg {
		case syscall.SIGHUP:
			err = hl.OpenDefault()
			if err != nil {
				log.Println(err)
				hl.Open("-")
			}

		default:
			hl.Close()
			break sigloop
		}
	}
}

func makeAttachmentFileName(year, month int, billing bool) string {
	prefix := "invoice"
	if billing {
		prefix = "billing"
	}
	return fmt.Sprintf("%s-%04d-%02d.csv", prefix, year, month)
}

func InvoiceServerHandler(w http.ResponseWriter, r *http.Request, path string, billing bool) {
	args := strings.Split(path, "/")
	if len(args) > 0 && args[len(args)-1] == `` {
		args = args[0:len(args)-1]
	}
	var (
		year, month int
		err error
	)
	switch len(args) {
	case 0:
		errorHandler(w, r, http.StatusNotFound)
		return
	case 1:
		// Assume current year
		year = time.Now().Year()
	case 2:
		year, err = strconv.Atoi(args[0])
		if err != nil {
			errorHandler(w, r, http.StatusBadRequest)
			return
		}
		args = args[1:]
	default:
		errorHandler(w, r, http.StatusBadRequest)
		return
	}

	month, err = strconv.Atoi(args[0])
	if err != nil {
		errorHandler(w, r, http.StatusBadRequest)
		return
	}

	q := r.URL.Query()

	invoices, err := ReadAWSInvoiceForMonth(year, month)
	if err != nil {
		log.Fatalf("error getting invoice for %d/%d: %v", year, month, err)
		errorHandler(w, r, http.StatusInternalServerError)
		return
	}

	if s := q.Get("id"); s != `` {
		ids := map[string]bool{}
		for _, id := range strings.Split(s, ",") {
			ids[id] = true
		}

		iv := []*AWSInvoice{}
		for _, i := range invoices {
			if _, ok := ids[i.BillingID]; ok {
				iv = append(iv, i)
			}
		}
		invoices = iv
	}

	header := w.Header()
	header.Set("Content-Type", "text/csv; charset=utf-8")
	header.Set("Content-Disposition", "attachment;filename=" + makeAttachmentFileName(year, month, billing))
	if year == time.Now().Year() && month == int(time.Now().Month()) {
		header.Set("Expires", "now")
		header.Set("Pragma", "no-cache")
                header.Set("Cache-control", "no-cache,no-store")
	}
	
	var result interface{}
	if billing {
		result = AWSBilling(invoices)
	} else {
		result = invoices
	}
	
	err = gocsv.Marshal(result, w)
	if err != nil {
		log.Fatal(err)
	}
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(status)

	fmt.Fprintf(w, `<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
  <head>
    <title>%d %s</title>
  </head>
  <body>
    <h1>%s</h1>
    <p></p>
  </body>
</html>`,
			status, http.StatusText(status), http.StatusText(status))
}

