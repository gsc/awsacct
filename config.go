package main

import (
	"context"
	"io/ioutil"
	"log"
	"net"	
	"gopkg.in/yaml.v2"
	"github.com/aws/aws-sdk-go-v2/aws"
	awsconfig "github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)	

type Config struct {
	Address		string		`yaml:"address"`
	Directory	string		`yaml:"directory"`
	BucketName	string		`yaml:"bucket_name"`
	AccessKeyID	string		`yaml:"access_key_id"`
	SecretAccessKey	string		`yaml:"secret_access_key"`
	Region		string		`yaml:"region"`
	AccessLog	string		`yaml:"access_log"`
	LogFormat	string		`yaml:"log_format"`
	TrustedIP	[]string	`yaml:"trusted_ip"`
	trustedNet	[]*net.IPNet
	
	accountID	string		`yaml:"-"`
	awsconf		aws.Config	`yaml:"-"`
}

func (cfg *Config) compileTrustedNets() {
	cfg.trustedNet = []*net.IPNet{}
	for _, ips := range cfg.TrustedIP {
		_, cidr, err := net.ParseCIDR(ips)
		if err != nil {
			if ip := net.ParseIP(ips); ip != nil {
				_, bits := ip.DefaultMask().Size()
				cidr = &net.IPNet{
					IP: ip,
					Mask: net.CIDRMask(bits, bits)}
			} else {
				log.Printf("can't parse %s: %v", ips, err)
				continue
			}
		}
		cfg.trustedNet = append(cfg.trustedNet, cidr)
	}
}

func (cfg *Config) IsTrustedIP(ips string) bool {
	if len(cfg.TrustedIP) > 0 {
		if cfg.trustedNet == nil {
			cfg.compileTrustedNets()
		}
		ip := net.ParseIP(ips)
		if ip != nil {
			for _, ipnet := range cfg.trustedNet {
				if ipnet.Contains(ip) {
					return true
				}
			}
		}
	}
	return false
}

var Conf = Config{
	Address: `:8080`,	
	TrustedIP: []string{ "127.0.0.1" ,},
	AccessLog: "-",
	LogFormat: `%a %l %u %t "%r" %>s %b "%{Referer}i" "%{User-Agent}i"`,
}

func ReadConfig(filename string) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("can't open configuration file: %v", err)
	}
	if err = yaml.Unmarshal(content, &Conf); err != nil {
		log.Fatal(err)
	}
	if Conf.Directory == `` {
		Conf.Directory = `.`
	}

	Conf.compileTrustedNets()

	ctx := context.TODO()
	Conf.awsconf, err = awsconfig.LoadDefaultConfig(ctx,
		awsconfig.WithRegion(Conf.Region),
		awsconfig.WithCredentialsProvider(credentials.StaticCredentialsProvider{
			Value: aws.Credentials{
				AccessKeyID: Conf.AccessKeyID,
				SecretAccessKey: Conf.SecretAccessKey,
				Source: "awsacct configuration",
			},
		}))

	if err != nil {
		log.Fatal(err)
	}

	identity, err := sts.NewFromConfig(Conf.awsconf).GetCallerIdentity(ctx, &sts.GetCallerIdentityInput{})
	if err != nil {
		log.Fatal(err)
	}
	Conf.accountID = aws.ToString(identity.Account)
	
	return
}

