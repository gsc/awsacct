package main

import (
	//	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"github.com/pborman/getopt/v2"
)

type BillOptSet struct {
	*getopt.Set
	InitArgs []string
	hflag bool
}

func NewBillOptSet(args []string) (optset *BillOptSet) {
	optset = new(BillOptSet)
	optset.Set = getopt.New()
	optset.InitArgs = args
	optset.SetProgram(filepath.Base(os.Args[0]) + " " + args[0])
	optset.FlagLong(&optset.hflag, "help", 'h', "Show this help")
	return
}

func (optset *BillOptSet) Parse() {
	if err := optset.Getopt(optset.InitArgs, nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		optset.PrintUsage(os.Stderr)
		os.Exit(1)
	}

	if optset.hflag {
		optset.PrintUsage(os.Stdout)
		os.Exit(0)
	}
}


type Action struct {
	Handler func ([]string)
	Help string
}

var actions map[string]Action = map[string]Action{
	"invoice":	Action{
		Handler: GetInvoiceActionHandler,
		Help: "Get full invoice for the given month",
	},
	"billing":	Action{
		Handler: BillingActionHandler,
		Help: "Get billing for the given month",
	},
	"server":	Action{
		Handler: ServerActionHandler,
		Help: "Run HTPP server",
	},
}

func HelpActionHandler(optset *getopt.Set, args []string) {
	commands := make([]string, len(actions))
	i := 0
	for com := range actions {
		commands[i] = com
		i++
	}
	sort.Strings(commands)
	optset.PrintUsage(os.Stdout)
	fmt.Printf("Available commands:\n")
	for _, com := range commands {
		fmt.Printf("    %-10s  %s\n", com, actions[com].Help)
	}
	fmt.Printf("To obtain a help on a particular command, run: %s COMMAND -h\n", filepath.Base(os.Args[0]))
}

func main() {
	optset := getopt.New()
	optset.SetProgram(filepath.Base(os.Args[0]))
	optset.SetParameters("[OPTIONS] COMMAND...")
	var (
		ConfigFile string
	)
	optset.FlagLong(&ConfigFile, "config", 'f', "Location of the configuration file")
	if err := optset.Getopt(os.Args, nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		optset.PrintUsage(os.Stderr)
		os.Exit(1)
	}

	log.SetPrefix(filepath.Base(os.Args[0]) + ": ")
	log.SetFlags(log.Lmsgprefix)

	filename := `awsacct.yml`
	if len(ConfigFile) > 0 {
		filename = ConfigFile
	}
	ReadConfig(filename)

	actions["help"] = Action{
		Handler: func (args []string) {
			HelpActionHandler(optset, args)
		},
		Help: "Show a short help summary",
	}

	args := optset.Args()
	if len(args) == 0 {
		log.Fatalf("command missing; try `%s help' for assistance", filepath.Base(os.Args[0]))
	}

	if act, ok := actions[args[0]]; ok {
		act.Handler(args)
		os.Exit(0)
	}
	log.Fatalf("unrecognized action; try `%s help' for assistance", filepath.Base(os.Args[0]))
}
