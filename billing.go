package main

import (
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
	"encoding/csv"
	"github.com/gocarina/gocsv"
	"github.com/pborman/getopt/v2"
)

type BillingID struct {
	ID map[string]bool
}

func (b *BillingID) Set(value string, opt getopt.Option) error {
	if b.ID == nil {
		b.ID = map[string]bool{}
	}
	b.ID[value] = true
	return nil
}

func (b *BillingID) String() string {
	ids := []string{}
	for i := range b.ID {
		ids = append(ids, i)
	}
	return strings.Join(ids, `,`)
}

func invoiceHandlerHelper(args []string) (invoices []*AWSInvoice, output io.Writer) {
	optset := NewBillOptSet(args)
	optset.SetParameters("[[YEAR] MONTH]")
	var (
		InputFileName string
		OutputFileName string
		Tab bool
		NoHeader bool
		billing BillingID
	)
	optset.FlagLong(&InputFileName, "file", 'f', "Input file name")
	optset.FlagLong(&OutputFileName, "output", 'o', "Output file name")
	optset.FlagLong(&Tab, "tab", 't', "Delimit fields by tab")
	optset.FlagLong(&NoHeader, "no-header", 'n', "Don't output header line")
	optset.FlagLong(&billing, "id", 'i', "Select only entries with this billing ID")
	optset.Parse()

	args = optset.Args()

	var err error

	if InputFileName != `` {
		if len(args) > 0 {
			log.Fatalf("extra arguments: either -f FILE or YEAR MONTH can be specified, not both")
		}
		invoices, err = ReadAWSInvoiceFromFile(InputFileName)
		if err != nil {
			log.Fatalf("error getting invoice from %s: %v", InputFileName, err)
		}
	} else {
		var (
			year, month int
		)
		switch len(args) {
		case 0:
			log.Fatal("not enough arguments (must give month or year and month)")
		case 1:
			// Assume current year
			year = time.Now().Year()
		case 2:
			year, err = strconv.Atoi(args[0])
			if err != nil {
				log.Fatal("invalid year")
			}
			args = args[1:]
		default:
			log.Fatal("too many arguments")
		}

		month, err = strconv.Atoi(args[0])
		if err != nil {
			log.Fatal("invalid month")
		}
		invoices, err = ReadAWSInvoiceForMonth(year, month)
		if err != nil {
			log.Fatalf("error getting invoice for %d/%d: %v", year, month, err)
		}
	}

	if len(billing.ID) > 0 {
		iv := []*AWSInvoice{}
		for _, i := range invoices {
			if _, ok := billing.ID[i.BillingID]; ok {
				iv = append(iv, i)
			}
		}
		invoices = iv
	}

	if OutputFileName != `` {
		file, err := os.Create(OutputFileName)
		if err != nil {
			log.Fatalf("can't create output file: %v", err)
		}
		defer file.Close()
		output = file
	} else {
		output = os.Stdout
	}

	if Tab {
		gocsv.SetCSVWriter(func(out io.Writer) *gocsv.SafeCSVWriter {
			writer := csv.NewWriter(out)
			writer.Comma = '\t'
			return gocsv.NewSafeCSVWriter(writer)
		})
	}

	if NoHeader {
		output = NewTailWriter(output, 1)
	}
	return
}

func GetInvoiceActionHandler(args []string) {
	invoices, output := invoiceHandlerHelper(args)
	err := gocsv.Marshal(invoices, output)
	if err != nil {
		log.Fatal(err)
	}
}

func BillingActionHandler(args []string) {
	invoices, output := invoiceHandlerHelper(args)

	billing := AWSBilling(invoices)

	err := gocsv.Marshal(billing, output)
	if err != nil {
		log.Fatal(err)
	}
}
